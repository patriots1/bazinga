
const cards = [
    {
        id:"1",
        title:"طرح خرگوش",
        imgsrc:"images/img1.JPG",
        imagenumber:1        
    },
    {
        id:"2",
        title:"طرح سگ",
        imgsrc:"images/img2.JPG",
        imagenumber:2            
    },
    {
        id:"3",
        title:"طرح گربه بازیگوش",
        imgsrc:"images/img3.JPG",
        imagenumber:3    
    },
    {
        id:"4",
        title:".طرح گاو زبان دراز.",
        imgsrc:"images/img4.JPG", 
        imagenumber:4            
    },
    {
        id:"5",
        title:"طرح کله شیر",
        imgsrc:"images/img5.JPG",
        imagenumber:5            
    },
    {
        id:"6",
        title:"طرح باربری",
        imgsrc:"images/img5.JPG",
        imagenumber:6        
    },
    {
        id:"7",
        title:"طرح تمساح",
        imgsrc:"images/img5.JPG",
        imagenumber:7        
    },
    {
        id:"8",
        title:"طرح یو اس پولو",
        imgsrc:"images/img5.JPG",
        imagenumber:8        
    },


]
export function getCards() {
    return cards;
  }
  
  export function getCard(id) {
    return cards.find(c => c._id === id);
  }