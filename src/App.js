import './App.css';

import NavBar from './components/navbar';
import Gallery from './components/gallery';
import Login from "./components/login";
import Register from "./components/register";
import Order from "./components/order";
import Home from "./components/home";
import NotFound from "./components/notFound";



import {Route, Switch , Redirect}  from 'react-router-dom';
import GalleryDetails from './components/galleryDetails';

function App() {
  return (
    <div>
     <NavBar />
     <div className="Content" >
       <Switch>
         <Route path='/login' component={Login} />
         <Route path='/register' component={Register} />
         <Route path='/order' component={Order} />
         <Route path='/gallery/:id' component={GalleryDetails} />
         <Route path='/gallery' component={Gallery} />
         <Route path='/not-found' component={NotFound} />
         <Route path="/" exact component={Home} />
         <Redirect to='/not-found' />
       </Switch>
     </div>
    </div>
  );
}

export default App;
