import React from 'react'

const GalleryDetails = (props) => {
   
        return ( 
          <div>
          <h1> {props.match.params.id}</h1>
          <img src={props.match.params.imgsrc} style={{height:'200px'}} className="card-img-top" alt="..."/>
          </div>
         );
    
}
 
export default GalleryDetails;