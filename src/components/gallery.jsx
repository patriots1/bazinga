import React, { Component } from 'react'
import Card from './card';
import Pagination from './common/pagination';
import {getCards} from '../services/fakeCardServices';


class Gallery extends Component {
    state = { 
       cards:[],
       pageSize:4,
     }
     componentDidMount() {
         const cards = getCards();
         this.setState({cards});
    }
    handlePageChange = () => {
    
    }

    render() { 
        const {length:count} = this.state.cards;

      

        return (
            <div>
            <div style={{display:'flex',justifyContent:'space-evenly',flexWrap:'wrap' }}>
                {this.state.cards.map(card =>
                    (<Card
                        key={card.id} id={card.id} title={card.title} imgSrc={card.imgsrc}
                    />
                ))}
            </div>
            <Pagination pageSize={this.state.pageSize} itemsCount={count} onPageChange={this.handlePageChange}  />
            </div>
      )
    }
}
export default Gallery;