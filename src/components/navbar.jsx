import React from 'react';
import { Link } from 'react-router-dom';
//import Logo from '../images/logo6.png';

const NavBar = () => {
    return (
   
        <nav className="navbar navbar-expand-lg navbar-light bg-light" >
        <div className="collapse navbar-collapse" id="navbarSupportedContent" style={{display:'flex',justifyContent:'space-between'}}>
          

          <div className="flex-item">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item active">
              <Link to="/" className="nav-link" >خانه <span className="sr-only">(current)</span></Link>
            </li>
            <li className="nav-item">
              <Link to="/gallery" className="nav-link" >نمونه طرح ها</Link>
            </li>   
            <li className="nav-item">
              <Link to="/order" className="nav-link" >سفارش طرح</Link>
            </li>   
            <li className="nav-item">
              <Link to="/register" className="nav-link" >ثبت نام</Link>
            </li>   
            <li className="nav-item">
              <Link to="/login" className="nav-link" >ورود</Link>
            </li>   
          </ul>
          </div>

          <div className="flex-item">  
            <nav className="nav-item" id="image" >
              <img alt="Logo" src="images/logo6.png" width="100" height="50"  />
            </nav> 
          </div>
         
         <div className="flex-item">
          <form className="form-inline my-2 my-lg-0">
            <input className="form-control mr-sm-2" type="search" placeholder="...جستجو" aria-label="Search"/>
            <button className="btn btn-outline-success my-2 my-sm-0" type="submit">!پیدا کن</button>
          </form>
         </div>

        </div>
      </nav>

      )}

export default NavBar ;
