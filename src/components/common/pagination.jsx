import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import _ from 'lodash';

const Pagination = (props) => {

  const pagesCount = props.itemsCount / props.pageSize;
  const pages = _.range(1,pagesCount+1);
  if(pages<=1)
    return null
  else{
    return (
  <ul className="pagination" style={{display:'flex', justifyContent:"center" , marginTop:'30px'}}>
    <li className="page-item"><Link className="page-link" to="#">Previous</Link></li>
    {pages.map(page => <li key={page} className="page-item"><Link className="page-link" to="#">{page}</Link></li>)}
   
    <li className="page-item"><Link className="page-link" to="#">Next</Link></li>
  </ul>
     );
}
}
export default Pagination;