import React from 'react';
import { Link } from 'react-router-dom';


const Card = (props) => {
  return (
    <div className="card" style={{width:'300px',marginTop:'20px'}}>
     <img src={props.imgSrc} style={{height:'200px'}} className="card-img-top" alt="..." />
      <div className="card-body">
        <h5 className="card-title">{props.title}</h5>
        <Link to={`/gallery/${props.id}`} className="btn btn-primary">جزییات</Link>
      </div>
    </div> );
}
export default Card;